#!/bin/sh

# usage:
#     ./mkicon.sh <input> <purpose>
# example:
#     ./mkicon.sh icon.svg launcher

mkicon() {
	file_in="$1"
	file_out="$2"
	case "$3" in
		action)
			case "$4" in
				ldpi)	width=18;;
				mdpi)	width=24;;
				hdpi)	width=36;;
				xhdpi)	width=48;;
			esac;;
		menu)
			case "$4" in
				ldpi)	width=24;;
				mdpi)	width=32;;
				hdpi)	width=48;;
				xhdpi)	width=64;;
			esac;;
		launcher)
			case "$4" in
				ldpi)	width=36;;
				mdpi)	width=48;;
				hdpi)	width=72;;
				xhdpi)	width=96;;
			esac;;
	esac
	rsvg-convert "$1" -o "$2" --width="${width}" --keep-aspect-ratio --format=png
}

g_file_in="$1"
g_file_out="${g_file_in##*/}"
g_file_out="${g_file_out%.*}.png"
for density in ldpi mdpi hdpi xhdpi; do
	mkdir -p "res/drawable-${density}"
	mkicon "${g_file_in}" "res/drawable-${density}/${g_file_out}" "$2" "${density}"
done

