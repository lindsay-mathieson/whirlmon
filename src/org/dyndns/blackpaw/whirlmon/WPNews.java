package org.dyndns.blackpaw.whirlmon;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

public class WPNews {
	public String blurb;
	public Date date;
	public long id;
	public String source;
	public String title;

	static public class List extends ArrayList<WPNews> {

		/**
		 * 
		 */
		private static final long serialVersionUID = -1555687293348785080L;
	}

	static public WPNews.List getNews(String apiKey) throws Exception {
		WPNews.List newsItems = new List();

		// Date Format
		// 2012-04-07T17:11:25+1000
		String gmtFormat = "yyyy-MM-dd'T'HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(gmtFormat);

		JSONObject jo = Json.getJSON(apiKey, "&get=news");
		JSONArray jaWatched = jo.getJSONArray("NEWS");
		for (int i = 0; i < jaWatched.length(); i++) {
			JSONObject joThread = jaWatched.getJSONObject(i);

			WPNews news = new WPNews();
			news.blurb = joThread.getString("BLURB");
			String sDate = joThread.getString("DATE");
			news.date = sdf.parse(sDate);
			news.id = joThread.getLong("ID");
			news.source = joThread.getString("SOURCE");
			news.title = joThread.getString("TITLE");

			newsItems.add(news);
		}

		return newsItems;
	}

}
